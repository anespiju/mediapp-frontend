import { Paciente } from 'src/app/_model/paciente';

export class Signos {
    idSignos: number;
    paciente: Paciente;
    fecha: string;
    temperatura: string;
    pulso: string;
    ritmoCardiaco: string;
}