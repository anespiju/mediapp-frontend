import { DetalleConsulta } from './detalleConsulta';
import { Especialidad } from './especialidad';
import { Paciente } from 'src/app/_model/paciente';
import { Medico } from './medico';

export class Consulta{
    idConsulta: number;
    paciente: Paciente;
    fecha: string;
    medico: Medico;
    especialidad: Especialidad;
    detalleConsulta: DetalleConsulta[];
}