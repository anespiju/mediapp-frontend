import { TOKEN_NAME } from './../../_shared/var.constant';
import { Component, OnInit } from '@angular/core';
import * as decode from 'jwt-decode';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {

  tipoRol: number;
  usuario: string;
  nombreRol: string;

  constructor() { }

  ngOnInit() {
    this.obtenerNombreRol();
  }

  obtenerNombreRol() {
    let tk = JSON.parse(sessionStorage.getItem(TOKEN_NAME));
    const decodedToken = decode(tk.access_token);
    let us = decodedToken.user_name;
    this.usuario = us.toUpperCase();
    this.nombreRol = decodedToken.authorities;
    decodedToken.authorities.forEach(element => {
      if(element === "ADMIN") {
        this.tipoRol = 1;  
      } else {
        this.tipoRol = 2;
      } 
    });
  }
}
