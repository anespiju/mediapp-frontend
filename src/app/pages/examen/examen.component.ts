import { ActivatedRoute } from '@angular/router';
import { ExamenService } from './../../_service/examen.service';
import { MatTableDataSource, MatPaginator, MatSort, MatSnackBar } from '@angular/material';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Examen } from '../../_model/examen';

@Component({
  selector: 'app-examen',
  templateUrl: './examen.component.html',
  styleUrls: ['./examen.component.css']
})
export class ExamenComponent implements OnInit {

  dataSource: MatTableDataSource<Examen>
  displayedColumns = ['idExamen', 'nombre', 'descripcion', 'acciones'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  mensaje: string;

  constructor(private examenService: ExamenService, public route: ActivatedRoute, public snackBar: MatSnackBar) { }

  ngOnInit() {
    this.examenService.examenCambio.subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });

    this.examenService.listarExamenes().subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });

    this.examenService.mensaje.subscribe(data => {
      this.snackBar.open(data, 'Aviso', { 
        duration: 2000, 
      });
    });
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

  eliminar(idExamen: number){
    this.examenService.eliminarExamen(idExamen).subscribe(data => {
      this.examenService.listarExamenes().subscribe(data => {
        this.examenService.examenCambio.next(data);
        this.examenService.mensaje.next('Se elimino examen');
      });
    });
  }

}
