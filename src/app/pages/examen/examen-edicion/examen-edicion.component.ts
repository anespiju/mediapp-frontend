import { ActivatedRoute, Router, Params } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
import { Examen } from './../../../_model/examen';
import { Component, OnInit } from '@angular/core';
import { ExamenService } from 'src/app/_service/examen.service';

@Component({
  selector: 'app-examen-edicion',
  templateUrl: './examen-edicion.component.html',
  styleUrls: ['./examen-edicion.component.css']
})
export class ExamenEdicionComponent implements OnInit {

  id: number;
  examen: Examen;
  form: FormGroup;
  edicion: boolean = false;

  constructor(private examenService: ExamenService, private route: ActivatedRoute, private router: Router) {
    this.examen = new Examen();

    this.form = new FormGroup({
      'id': new FormControl(0),
      'nombre': new FormControl(''),
      'descripcion': new FormControl(''),
    });
  }

  ngOnInit() {
    this.examen = new Examen();
    this.route.params.subscribe((params: Params) =>{
      this.id = params['idExamen'];
      this.edicion = params['idExamen']!= null;
      this.initForm();
    });
  }

  initForm(){
    console.log(this.edicion);
    if(this.edicion){
      this.examenService.listarExamenPorId(this.id).subscribe(data => {
        let id = data.idExamen;
        let nombre = data.nombre;
        let descripcion = data.descripcion;
        this.form = new FormGroup({
          'id': new FormControl(id),
          'nombre': new FormControl(nombre),
          'descripcion': new FormControl(descripcion),
        });
      });
    }
  }

  operar(){
    this.examen.idExamen = this.form.value['id'];
    this.examen.nombre = this.form.value['nombre'];
    this.examen.descripcion = this.form.value['descripcion'];

    if(this.examen != null && this.examen.idExamen > 0 ){
      this.examenService.modificarExamen(this.examen).subscribe(data => {
        this.examenService.listarExamenes().subscribe(examenes => {
          this.examenService.examenCambio.next(examenes);
          this.examenService.mensaje.next("Se modifico datos del examen");
        });
      });
    } else{
      this.examenService.registrarExamen(this.examen).subscribe(data => {
        this.examenService.listarExamenes().subscribe(examenes => {
          this.examenService.examenCambio.next(examenes);
          this.examenService.mensaje.next("Se registro examen");
        });
      });
    }

    this.router.navigate(['examen']);
  }
}
