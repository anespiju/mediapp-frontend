import { Paciente } from './../../../../_model/paciente';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Component, OnInit, Inject } from '@angular/core';
import { PacienteService } from 'src/app/_service/paciente.service';

@Component({
  selector: 'app-dialogo-paciente',
  templateUrl: './dialogo-paciente.component.html',
  styleUrls: ['./dialogo-paciente.component.css']
})
export class DialogoPacienteComponent implements OnInit {

  paciente: Paciente;

  constructor(private dialogRef: MatDialogRef<DialogoPacienteComponent>, @Inject(MAT_DIALOG_DATA) public data: Paciente, private pacienteService: PacienteService) { }

  ngOnInit() {
    this.paciente = new Paciente();
    this.paciente.idPaciente = this.data.idPaciente;
    this.paciente.nombres = this.data.nombres;
    this.paciente.apellidos = this.data.apellidos;
    this.paciente.dni = this.data.dni;
    this.paciente.direccion = this.data.direccion;
    this.paciente.telefono = this.data.telefono;
  }

  cancelar() {
    this.dialogRef.close();
  }

  agregar() {
    this.pacienteService.registrarPaciente(this.paciente).subscribe(data => {
      this.pacienteService.listarPacientes().subscribe(pacientes => {
        this.pacienteService.pacienteCambio.next(pacientes);
      });
    });

    this.pacienteService.pacienteCambio.subscribe(data => {
      let ultiPac = data[data.length - 1];
      this.pacienteService.ultimoPaciente = ultiPac;
    });
    this.dialogRef.close();
  }
}
