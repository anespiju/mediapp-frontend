import { DialogoPacienteComponent } from './dialogo-paciente/dialogo-paciente.component';
import { MatSnackBar, MatDialog } from '@angular/material';
import { SignosService } from './../../../_service/signos.service';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { PacienteService } from './../../../_service/paciente.service';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Paciente } from './../../../_model/paciente';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Signos } from 'src/app/_model/signos';

@Component({
  selector: 'app-signos-edicion',
  templateUrl: './signos-edicion.component.html',
  styleUrls: ['./signos-edicion.component.css']
})
export class SignosEdicionComponent implements OnInit {

  form: FormGroup;

  myControlPaciente: FormControl = new FormControl();

  pacientes: Paciente[] = [];
  signos: Signos;

  fechaSeleccionada: Date = new Date();
  maxFecha: Date = new Date();

  id: number;
  temperatura: string;
  pulso: string;
  ritmoCardiaco: string;

  mensaje: string;

  filteredOptions: Observable<any[]>;
  filteredOptionsMedico: Observable<any[]>;

  pacienteSeleccionado: Paciente;
  pacienteNuevo: Paciente;

  edicion: boolean = false;
  nuevoPaciente: boolean = false;

  constructor(private builder: FormBuilder, private route: ActivatedRoute, private router: Router, private pacienteService: PacienteService, private signosService: SignosService, private dialog: MatDialog, private snackBar: MatSnackBar) {
    this.form = builder.group({
      'id': new FormControl(0),
      'paciente': this.myControlPaciente,
      'fecha': new FormControl(new Date()),
      'temperatura': new FormControl(''),
      'pulso': new FormControl(''),
      'ritmoCardiaco': new FormControl('')
    });
  }

  ngOnInit() {
    this.listarPacientes();
    this.filteredOptions = this.myControlPaciente.valueChanges.pipe(map(val => this.filter(val)));

    this.signos = new Signos();
    this.route.params.subscribe((params: Params) => {
      this.id = params['idSignos'];
      this.edicion = params['idSignos'] != null;
      this.initForm();
    });
  }

  initForm() {

    if (this.edicion) {
      this.signosService.listarSignosPorId(this.id).subscribe(data => {
        this.form = new FormGroup({
          'id': new FormControl(data.idSignos),
          'paciente': new FormControl(data.paciente),
          'fecha': new FormControl(data.fecha),
          'temperatura': new FormControl(data.temperatura),
          'pulso': new FormControl(data.pulso),
          'ritmoCardiaco': new FormControl(data.ritmoCardiaco)
        });
      });
    }
  }

  listarPacientes() {
    this.pacienteService.listarPacientes().subscribe(data => {
      this.pacientes = data;
    });
  }

  filter(val: any) {
    if (val != null && val.idPaciente > 0) {
      return this.pacientes.filter(option =>
        option.nombres.toLowerCase().includes(val.nombres.toLowerCase()) || option.apellidos.toLowerCase().includes(val.apellidos.toLowerCase()) || option.dni.includes(val.dni));
    } else {
      return this.pacientes.filter(option =>
        option.nombres.toLowerCase().includes(val.toLowerCase()) || option.apellidos.toLowerCase().includes(val.toLowerCase()) || option.dni.includes(val));
    }
  }

  displayFn(val: Paciente) {
    return val ? `${val.nombres} ${val.apellidos}` : val;
  }

  seleccionarPaciente(e: any) {
    this.pacienteSeleccionado = e.option.value;
  }

  operar() {

    if (this.edicion) {
      //actualizar
      this.signos = new Signos();
      this.signos.idSignos = this.form.value['id'];
      this.signos.paciente = this.form.value['paciente'];
      this.signos.fecha = this.form.value['fecha'];
      this.signos.temperatura = this.form.value['temperatura'];
      this.signos.pulso = this.form.value['pulso'];
      this.signos.ritmoCardiaco = this.form.value['ritmoCardiaco'];

      this.signosService.modificarSignos(this.signos).subscribe(data => {
        this.signosService.listarSignos().subscribe(signos => {
          this.signosService.signosCambio.next(signos);
          this.signosService.mensajeCambio.next('Se modificaron los datos');
        });
      });
    } else {
      //registrar
      this.signos = new Signos();
      this.signos.paciente = this.form.value['paciente'];
      let tzoffset = (this.form.value['fecha']).getTimezoneOffset() * 60000;
      let localISOTime = (new Date(Date.now() - tzoffset)).toISOString();
      this.signos.fecha = localISOTime;
      this.signos.temperatura = this.form.value['temperatura'];
      this.signos.pulso = this.form.value['pulso'];
      this.signos.ritmoCardiaco = this.form.value['ritmoCardiaco'];

      this.signosService.registrarSignos(this.signos).subscribe(data => {
        this.signosService.listarSignos().subscribe(signos => {
          this.signosService.signosCambio.next(signos);
          this.signosService.mensajeCambio.next('Se registro');
        });
      });
    }
    this.router.navigate(['signos']);
  }

  openDialog(paciente: Paciente) {
    let pac = paciente != null ? paciente : new Paciente();
    this.dialog.open(DialogoPacienteComponent, {
      width: '250px',
      disableClose: true,
      data: pac
    });
    this.nuevoPaciente = true;
  }
}