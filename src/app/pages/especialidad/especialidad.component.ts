import { ActivatedRoute } from '@angular/router';
import { EspecialidadService } from './../../_service/especialidad.service';
import { MatTableDataSource, MatPaginator, MatSort, MatSnackBar } from '@angular/material';
import { Especialidad } from './../../_model/especialidad';
import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-especialidad',
  templateUrl: './especialidad.component.html',
  styleUrls: ['./especialidad.component.css']
})
export class EspecialidadComponent implements OnInit {

  dataSource: MatTableDataSource<Especialidad>
  displayedColumns = ['idEspecialidad', 'nombre', 'acciones'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  mensaje: string;
  cantidad: number;

  constructor(private especialidadService: EspecialidadService, public route: ActivatedRoute, public snackBar: MatSnackBar) { }

  ngOnInit() {
    this.especialidadService.especialidadesCambio.subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });

    this.especialidadService.listarEspecialidades().subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });

    this.especialidadService.mensaje.subscribe(data => {
      this.snackBar.open(data, 'Aviso', { 
        duration: 2000, 
      });
    });
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

  eliminar(idEspecialidad: number){
    this.especialidadService.eliminarEspecialidad(idEspecialidad).subscribe(data => {
      this.especialidadService.listarEspecialidades().subscribe(data => {
        this.especialidadService.especialidadesCambio.next(data);
        this.especialidadService.mensaje.next('Se elimino examen');
      });
    });
  }


}
