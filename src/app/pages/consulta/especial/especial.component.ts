import { ConsultaListaExamen } from './../../../_model/consultaListaExamen';
import { MatSnackBar } from '@angular/material';
import { MedicoService } from './../../../_service/medico.service';
import { EspecialidadService } from './../../../_service/especialidad.service';
import { PacienteService } from 'src/app/_service/paciente.service';
import { ExamenService } from './../../../_service/examen.service';
import { ConsultaService } from './../../../_service/consulta.service';
import { Especialidad } from './../../../_model/especialidad';
import { Examen } from './../../../_model/examen';
import { Consulta } from './../../../_model/consulta';
import { Paciente } from './../../../_model/paciente';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Medico } from 'src/app/_model/medico';
import { DetalleConsulta } from 'src/app/_model/detalleConsulta';
import { Observable, } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

@Component({
  selector: 'app-especial',
  templateUrl: './especial.component.html',
  styleUrls: ['./especial.component.css']
})
export class EspecialComponent implements OnInit {

  form: FormGroup;

  myControlPaciente: FormControl = new FormControl();
  myControlMedico: FormControl = new FormControl();

  pacientes: Paciente[] = [];
  especialidades: Especialidad[] = [];
  medicos: Medico[] = [];
  consulta: Consulta;
  examenes: Examen[] = [];

  detalleConsulta: DetalleConsulta[] = [];
  examenesSeleccionados: Examen[] = [];

  diagnostico: string;
  tratamiento: string;
  fechaSeleccionada: Date = new Date();
  maxFecha: Date = new Date();

  mensaje: string;

  filteredOptions: Observable<any[]>;
  filteredOptionsMedico: Observable<any[]>;

  pacienteSeleccionado: Paciente;
  medicoSeleccionado: Medico;
  especialidadSeleccionado: Especialidad;
  examenSeleccionado: Examen;

  constructor(private builder: FormBuilder, private consultaService: ConsultaService, private examenService: ExamenService, private pacienteService: PacienteService, private especialidadService: EspecialidadService, private medicoService: MedicoService, private snackBar: MatSnackBar) {
    this.form = builder.group({
      'paciente': this.myControlPaciente,
      'especialidad': new FormControl(),
      'medico': this.myControlMedico,
      'fecha': new FormControl(new Date()),
      'diagnostico': new FormControl(''),
      'tratamiento': new FormControl('')
    });
  }

  ngOnInit() {
    this.listarPacientes();
    this.listarEspecialidades();
    this.listarMedicos();
    this.listarExamenes();

    this.filteredOptions = this.myControlPaciente.valueChanges.pipe(map(val => this.filter(val)));
    this.filteredOptionsMedico = this.myControlMedico.valueChanges.pipe(map(val => this.filterMedico(val)));
  }

  filter(val: any) {
    if (val != null && val.idPaciente > 0) {
      return this.pacientes.filter(option =>
        option.nombres.toLowerCase().includes(val.nombres.toLowerCase()) || option.apellidos.toLowerCase().includes(val.apellidos.toLowerCase()) || option.dni.includes(val.dni));
    } else {
      return this.pacientes.filter(option =>
        option.nombres.toLowerCase().includes(val.toLowerCase()) || option.apellidos.toLowerCase().includes(val.toLowerCase()) || option.dni.includes(val));
    }
  }

  filterMedico(val: any) {
    if (val != null && val.idMedico > 0) {
      return this.medicos.filter(option =>
        option.nombres.toLowerCase().includes(val.nombres.toLowerCase()) || option.apellidos.toLowerCase().includes(val.apellidos.toLowerCase()) || option.cmp.includes(val.cmp));
    } else {
      return this.medicos.filter(option =>
        option.nombres.toLowerCase().includes(val.toLowerCase()) || option.apellidos.toLowerCase().includes(val.toLowerCase()) || option.cmp.includes(val));
    }
  }

  displayFn(val: Paciente) {
    return val ? `${val.nombres} ${val.apellidos}` : val;
  }

  displayFnMedico(val: Medico) {
    return val ? `${val.nombres} ${val.apellidos}` : val;
  }

  listarPacientes() {
    this.pacienteService.listarPacientes().subscribe(data => {
      this.pacientes = data;
    });
  }

  listarEspecialidades() {
    this.especialidadService.listarEspecialidades().subscribe(data => {
      this.especialidades = data;
    });
  }

  listarMedicos() {
    this.medicoService.listarMedicos().subscribe(data => {
      this.medicos = data;
    });
  }

  listarExamenes() {
    this.examenService.listarExamenes().subscribe(data => {
      this.examenes = data;
    });
  }

  seleccionarPaciente(e: any) {
    this.pacienteSeleccionado = e.option.value;
  }

  seleccionarMedico(e: any) {
    this.medicoSeleccionado = e.option.value;
  }

  estadoBotonRegistrar() {
    return (this.detalleConsulta.length === 0 || this.pacienteSeleccionado === null || this.especialidadSeleccionado === null || this.medicoSeleccionado === null);
  }

  agregar() {
    if (this.diagnostico != null && this.tratamiento != null) {
      let det = new DetalleConsulta();
      det.diagnostico = this.diagnostico;
      det.tratamiento = this.tratamiento;
      this.detalleConsulta.push(det);

      this.diagnostico = null;
      this.tratamiento = null;
    } else {
      this.mensaje = 'Debe agregar un diagnostico y tratamiento';
      this.snackBar.open(this.mensaje, "Aviso", { duration: 2000 });
    }
  }

  agregarExamen() {
    if (this.examenSeleccionado) {
      let cont = 0;
      for (let i = 0; i < this.examenesSeleccionados.length; i++) {
        let examen = this.examenesSeleccionados[i];
        if (examen.idExamen == this.examenSeleccionado.idExamen) {
          cont++;
          break;
        }
      }
      if (cont > 0) {
        this.mensaje = 'El examen se encuentra en la lista';
        this.snackBar.open(this.mensaje, 'Aviso', { duration: 2000 });
      } else {
        this.examenesSeleccionados.push(this.examenSeleccionado);
      }
    } else {
      this.mensaje = 'Debe agregar un examen';
      this.snackBar.open(this.mensaje, 'Aviso', { duration: 2000 });
    }
  }

  aceptar() {
    this.consulta = new Consulta();
    this.consulta.paciente = this.form.value['paciente'];
    this.consulta.especialidad = this.form.value['especialidad'];
    this.consulta.medico = this.form.value['medico'];
    this.consulta.detalleConsulta = this.detalleConsulta;
    let tzoffset = (this.form.value['fecha']).getTimezoneOffset()*60000;
    let localISOTime = (new Date(Date.now() - tzoffset)).toISOString();
    this.consulta.fecha = localISOTime;

    let consultaListaExamen = new ConsultaListaExamen();
    consultaListaExamen.consulta = this.consulta;
    consultaListaExamen.lstExamen = this.examenesSeleccionados;

    this.consultaService.registrar(consultaListaExamen).subscribe(data => {
      this.snackBar.open("Se registro", "Aviso", { duration: 2000 });

      setTimeout(() => {
        this.limpiarControles();
      }, 2000);
    });
  }

  limpiarControles() {
    this.detalleConsulta = [];
    this.examenesSeleccionados = [];
    this.diagnostico = '';
    this.tratamiento = '';
    this.pacienteSeleccionado = null;
    this.especialidadSeleccionado = null;
    this.medicoSeleccionado = null;
    this.fechaSeleccionada = new Date();
    this.fechaSeleccionada.setHours(0);
    this.fechaSeleccionada.setMinutes(0);
    this.fechaSeleccionada.setSeconds(0);
    this.fechaSeleccionada.setMilliseconds(0);
    this.mensaje = '';
    this.consulta = new Consulta();
  }

  removerDiagnostico(index: number) {
    this.detalleConsulta.splice(index, 1);
  }

  removerExamen(index: number) {
    this.examenesSeleccionados.splice(index, 1);
  }
}
